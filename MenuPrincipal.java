import java.util.Scanner;
import java.io.*;
import java.util.*;
class MenuPrincipal{
	
	public static void main (String...Args) throws IOException,InterruptedException
	{  
	  int opcion;
	  float numfib,segundos;
	 
	  do{
		Scanner lee=new Scanner(System.in);
        System.out.println("===============\tM E N U\t===============");
	    System.out.println("   1.- Serie fibonacci                ");
	    System.out.println("   2.- Horas en determinados segundos ");		
		System.out.println("   3.- Salir                          ");	
		
		opcion=lee.nextInt();
		
		switch(opcion){
		case 1:
		       System.out.println("Cuantos numeros quieres ver?"); 
		       numfib=lee.nextInt();
			   Operaciones.Fibonacci(numfib);
		       break;
		case 2: 
		       System.out.println("Ingresa la cantidad de segundos"); 
		       segundos=lee.nextInt();
		       Operaciones.Segundos(segundos); 
		       break;   
        case 3:                 
		        break;   		
  
        default: System.out.println("opcion no valida");
		        break;   				
			}
			
		Thread.sleep(2000);
	    //new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor(); 
		
	  }while (opcion != 3);
	}
	
}